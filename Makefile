SRC = src/capitulo_
OBJ = obj/capitulo_
BIN = bin/capitulo_

all: capitulo1 capitulo2 capitulo3 capitulo4 capitulo5 

clean:clean_c1 clean_c2 clean_c3 clean_c4 clean_c5


################################## CAPITULO 1 ######################################

capitulo1:listing-1.1
clean_c1:
	rm obj/capitulo_1/*.o
	rm bin/capitulo_1/reciproco

listing-1.1: obj/capitulo_1/main.o obj/capitulo_1/reciprocal.o
	g++ obj/capitulo_1/main.o obj/capitulo_1/reciprocal.o -o bin/capitulo_1/reciproco
obj/capitulo_1/main.o:
	gcc -c -Isrc/capitulo_1 src/capitulo_1/main.c -o obj/capitulo_1/main.o
obj/capitulo_1/reciprocal.o:
	g++ -c -Isrc/capitulo_1 src/capitulo_1/reciprocal.cpp -o obj/capitulo_1/reciprocal.o



################################## CAPITULO 2 ######################################

capitulo2: listing-2.1 listing-2.2 listing-2.3 listing-2.4 listing-2.5 listing-2.6 listing-2.7-2.8 listing-2.9



listing-2.1:arglist.o
	gcc -o bin/capitulo_2/2.1/arglist obj/capitulo_2/2.1/arglist.o

arglist.o:
	gcc -c src/capitulo_2/2.1/arglist.c -o obj/capitulo_2/2.1/arglist.o

clean2.1:
	rm -f bin/capitulo_2/2.1/arglist obj/capitulo_2/2.1/arglist.o



listing-2.2:getopt_long.o
	gcc -o bin/capitulo_2/2.2/getopt_long obj/capitulo_2/2.2/getopt_long.o
getopt_long.o:
	gcc -c src/capitulo_2/2.2/getopt_long.c -o obj/capitulo_2/2.2/getopt_long.o
clean2.2:
	rm -f bin/capitulo_2/2.2/getopt_long obj/capitulo_2/2.2/getopt_long.o


listing-2.3:print-env.o
	gcc -o bin/capitulo_2/2.3/print-env obj/capitulo_2/2.3/print-env.o
print-env.o:
	gcc -c src/capitulo_2/2.3/print-env.c -o obj/capitulo_2/2.3/print-env.o
clean2.3:
	rm -f bin/capitulo_2/2.3/print-env obj/capitulo_2/2.3/print-env.o




listing-2.4:client.o
	gcc -o bin/capitulo_2/2.4/client obj/capitulo_2/2.4/client.o
client.o:
	gcc -c src/capitulo_2/2.4/client.c -o obj/capitulo_2/2.4/client.o
clean2.4:
	rm -f bin/capitulo_2/2.4/client obj/capitulo_2/2.4/client.o





listing-2.5:temp_file.o
	gcc -o bin/capitulo_2/2.5/temp_file obj/capitulo_2/2.5/temp_file.o
temp_file.o:
	gcc -c src/capitulo_2/2.5/temp_file.c -o obj/capitulo_2/2.5/temp_file.o
clean2.5:
	rm -f bin/capitulo_2/2.5/temp_file obj/capitulo_2/2.5/temp_file.o



listing-2.6:readfile.o
	gcc -o bin/capitulo_2/2.6/readfile obj/capitulo_2/2.6/readfile.o
readfile.o:
	gcc -c src/capitulo_2/2.6/readfile.c -o obj/capitulo_2/2.6/readfile.o
clean2.6:
	rm -f bin/capitulo_2/2.6/readfile obj/capitulo_2/2.6/readfile.o



listing-2.7-2.8: app.o test.o
	gcc -o bin/capitulo_2/2.7/app obj/capitulo_2/2.8/app.o -Lsrc/capitulo_2/2.7 -ltest

test.o:
	gcc -c -o obj/capitulo_2/2.7/test.o src/capitulo_2/2.7/test.c
	ar cr src/capitulo_2/2.7/libtest.a obj/capitulo_2/2.7/test.o

app.o:
	gcc -c -o obj/capitulo_2/2.8/app.o src/capitulo_2/2.8/app.c

clean2.7-2.8:
	rm -f src/capitulo_2/2.7/libtest.a obj/capitulo_2/2.7/test.o obj/capitulo_2/2.8/app.o bin/capitulo_2/2.7/app



listing-2.9: tifftest.o
	gcc -o bin/capitulo_2/2.9/tifftest obj/capitulo_2/2.9/tifftest.o -ltiff 

tifftest.o:
	gcc -c src/capitulo_2/2.9/tifftest.c -o obj/capitulo_2/2.9/tifftest.o -ltiff
clean2.9:
	rm -f bin/capitulo_2/2.9/tifftest obj/capitulo_2/2.9/tifftest.o

clean_c2:
	rm obj/capitulo_2/2.*/*.o
	rm bin/capitulo_2/2.*/*
	rm src/capitulo_2/2.7/*.a




################################## CAPITULO 3 #######################################

capitulo3: listing-3.1 listing-3.2 listing-3.3 listing-3.4 listing-3.5 listing-3.6 listing-3.7

listing-3.1: obj/capitulo_3/3.1/print-pid.o
	gcc src/capitulo_3/3.1/print-pid.c -o bin/capitulo_3/3.1/print-pid
obj/capitulo_3/3.1/print-pid.o:
	gcc src/capitulo_3/3.1/print-pid.c -o obj/capitulo_3/3.1/print-pid.o


listing-3.2: obj/capitulo_3/3.2/system.o
	gcc src/capitulo_3/3.2/system.c -o bin/capitulo_3/3.2/sistema
obj/capitulo_3/3.2/system.o:
	gcc src/capitulo_3/3.2/system.c -o obj/capitulo_3/3.2/system.o


listing-3.3: obj/capitulo_3/3.3/fok.o
	gcc src/capitulo_3/3.3/fork.c -o bin/capitulo_3/3.3/fork
obj/capitulo_3/3.3/fok.o:
	gcc src/capitulo_3/3.3/fork.c -o obj/capitulo_3/3.3/fork.o


listing-3.4: obj/capitulo_3/3.4/fok-exec.o
	gcc src/capitulo_3/3.4/fork-exec.c -o bin/capitulo_3/3.4/fork-exec
obj/capitulo_3/3.4/fok-exec.o:
	gcc src/capitulo_3/3.4/fork-exec.c -o obj/capitulo_3/3.4/fok-exec.o


listing-3.5: obj/capitulo_3/3.5/sigusr1.o
	gcc src/capitulo_3/3.5/sigusr1.c -o bin/capitulo_3/3.5/sigusr1
obj/capitulo_3/3.5/sigusr1.o:
	gcc src/capitulo_3/3.5/sigusr1.c -o obj/capitulo_3/3.5/sigusr1.o


listing-3.6: obj/capitulo_3/3.6/zombie.o
	gcc src/capitulo_3/3.6/zombie.c -o bin/capitulo_3/3.6/zombie
obj/capitulo_3/3.6/zombie.o:
	gcc src/capitulo_3/3.6/zombie.c -o obj/capitulo_3/3.6/zombie.o


listing-3.7: obj/capitulo_3/3.7/sigchld.o
	gcc src/capitulo_3/3.7/sigchld.c -o bin/capitulo_3/3.7/sigchld
obj/capitulo_3/3.7/sigchld.o:
	gcc src/capitulo_3/3.7/sigchld.c -o obj/capitulo_3/3.7/sigchld.o

clean_c3:
	rm obj/capitulo_3/3.*/*.o
	rm bin/capitulo_3/3.*/*

########################################## CAPITULO 4 ########################################

capitulo4: listing-4.1 listing-4.2 listing-4.3 listing-4.4 listing-4.5 listing-4.6 listing-4.7 listing-4.8 listing-4.9 listing-4.10 listing-4.11 listing-4.12 listing-4.13 listing-4.14 listing-4.15



listing-4.1:thread-create.o
	gcc -o bin/capitulo_4/4.1/thread-create obj/capitulo_4/4.1/thread-create.o -lpthread
thread-create.o:
	gcc -c src/capitulo_4/4.1/thread-create.c -o obj/capitulo_4/4.1/thread-create.o



listing-4.2:thread-create2.o
	gcc -o bin/capitulo_4/4.2/thread-create2 obj/capitulo_4/4.2/thread-create2.o -lpthread
thread-create2.o:
	gcc -c src/capitulo_4/4.2/thread-create2.c -o obj/capitulo_4/4.2/thread-create2.o



listing-4.3:thread-create3.o
	gcc -o bin/capitulo_4/4.3/thread-create3 obj/capitulo_4/4.3/thread-create3.o -lpthread
thread-create3.o:
	gcc -c src/capitulo_4/4.3/thread-create3.c -o obj/capitulo_4/4.3/thread-create3.o

listing-4.4:primes.o
	gcc -o bin/capitulo_4/4.4/primes obj/capitulo_4/4.4/primes.o -lpthread
primes.o:
	gcc -c src/capitulo_4/4.4/primes.c -o obj/capitulo_4/4.4/primes.o

listing-4.5:detached.o
	gcc -o bin/capitulo_4/4.5/detached obj/capitulo_4/4.5/detached.o -lpthread
detached.o:
	gcc -c src/capitulo_4/4.5/detached.c -o obj/capitulo_4/4.5/detached.o

listing-4.6:critical-section.o
	gcc -o bin/capitulo_4/4.6/critical-section obj/capitulo_4/4.6/critical-section.o -lpthread
critical-section.o:
	gcc -c src/capitulo_4/4.6/critical-section.c -o obj/capitulo_4/4.6/critical-section.o


listing-4.7:tsd.o
	gcc -o bin/capitulo_4/4.7/tsd obj/capitulo_4/4.7/tsd.o -lpthread
tsd.o:
	gcc -c src/capitulo_4/4.7/tsd.c -o obj/capitulo_4/4.7/tsd.o


listing-4.8:cleanup.o
	gcc -o bin/capitulo_4/4.8/cleanup obj/capitulo_4/4.8/cleanup.o -lpthread
cleanup.o:
	gcc -c src/capitulo_4/4.8/cleanup.c -o obj/capitulo_4/4.8/cleanup.o


listing-4.9: $(OBJ)4/4.9/cxx-exit.o
	g++ $(OBJ)4/4.9/cxx-exit.o -o $(BIN)4/4.9/cxx-exit -lpthread

$(OBJ)4/4.9/cxx-exit.o: $(SRC)4/4.9/cxx-exit.cpp
	g++ -c $(SRC)4/4.9/cxx-exit.cpp -o $(OBJ)4/4.9/cxx-exit.o






listing-4.10:job-queue1.o
	gcc -o bin/capitulo_4/4.10/job-queue1 obj/capitulo_4/4.10/job-queue1.o -lpthread
job-queue1.o:
	gcc -c src/capitulo_4/4.10/job-queue1.c -o obj/capitulo_4/4.10/job-queue1.o




listing-4.11: $(OBJ)4/4.11/job_queue2.o
	gcc $(OBJ)4/4.11/job_queue2.o -o $(BIN)4/4.11/job_queue2 -lpthread

$(OBJ)4/4.11/job_queue2.o: $(SRC)4/4.11/job_queue2.c
	gcc -c $(SRC)4/4.11/job_queue2.c -o $(OBJ)4/4.11/job_queue2.o

listing-4.12: $(OBJ)4/4.12/job_queue3.o
	gcc $(OBJ)4/4.12/job_queue3.o -o $(BIN)4/4.12/job_queue3 -lpthread

$(OBJ)4/4.12/job_queue3.o: $(SRC)4/4.12/job_queue3.c
	gcc -c $(SRC)4/4.12/job_queue3.c -o $(OBJ)4/4.12/job_queue3.o

listing-4.13: $(OBJ)4/4.13/spin-condvar.o
	gcc $(OBJ)4/4.13/spin-condvar.o -o $(BIN)4/4.13/spin-condvar -lpthread

$(OBJ)4/4.13/spin-condvar.o: $(SRC)4/4.13/spin-condvar.c
	gcc -c $(SRC)4/4.13/spin-condvar.c -o $(OBJ)4/4.13/spin-condvar.o

listing-4.14: $(OBJ)4/4.14/condvar.o
	gcc $(OBJ)4/4.14/condvar.o -o $(BIN)4/4.14/condvar -lpthread

$(OBJ)4/4.14/condvar.o: $(SRC)4/4.14/condvar.c
	gcc -c $(SRC)4/4.14/condvar.c -o $(OBJ)4/4.14/condvar.o


listing-4.15:thread-pid.o
	gcc -o bin/capitulo_4/4.15/thread-pid obj/capitulo_4/4.15/thread-pid.o -lpthread
thread-pid.o:
	gcc -c src/capitulo_4/4.15/thread-pid.c -o obj/capitulo_4/4.15/thread-pid.o

clean_c4: 
	rm obj/capitulo_4/4.*/*.o
	rm bin/capitulo_4/4.*/*

################################### CAPITULO 5 #####################################


capitulo5: listing-5.1 listing-5.2 listing-5.3 listing-5.4 listing-5.5 listing-5.7 listing-5.8 listing-5.9 listing-5.10 listing-5.11 listing-5.12

listing-5.1: $(OBJ)5/5.1/smh.o
	gcc $(OBJ)5/5.1/smh.o -o $(BIN)5/5.1/smh

$(OBJ)5/5.1/smh.o: $(SRC)5/5.1/smh.c
	gcc -c $(SRC)5/5.1/smh.c -o $(OBJ)5/5.1/smh.o

listing-5.2: $(OBJ)5/5.2/sem_all_deall.o
	gcc $(OBJ)5/5.2/sem_all_deall.o -o $(BIN)5/5.2/sem_all_deall

$(OBJ)5/5.2/sem_all_deall.o: $(SRC)5/5.2/sem_all_deall.c
	gcc -c $(SRC)5/5.2/sem_all_deall.c -o $(OBJ)5/5.2/sem_all_deall.o

listing-5.3: $(OBJ)5/5.3/sem_init.o
	gcc $(OBJ)5/5.3/sem_init.o -o $(BIN)5/5.3/sem_init

$(OBJ)5/5.3/sem_init.o: $(SRC)5/5.3/sem_init.c
	gcc -c $(SRC)5/5.3/sem_init.c -o $(OBJ)5/5.3/sem_init.o

listing-5.4: $(OBJ)5/5.4/sem_pv.o
	gcc $(OBJ)5/5.4/sem_pv.o -o $(BIN)5/5.4/sem_pv

$(OBJ)5/5.4/sem_pv.o: $(SRC)5/5.4/sem_pv.c
	gcc -c $(SRC)5/5.4/sem_pv.c -o $(OBJ)5/5.4/sem_pv.o

listing-5.5: $(OBJ)5/5.5/mmap-write.o
	gcc $(OBJ)5/5.5/mmap-write.o -o $(BIN)5/5.5/mmap-write

$(OBJ)5/5.5/mmap-write.o: $(SRC)5/5.5/mmap-write.c
	gcc -c $(SRC)5/5.5/mmap-write.c -o $(OBJ)5/5.5/mmap-write.o

listing-5.6: $(OBJ)5/5.6/mmap-read.o
	gcc $(OBJ)5/5.6/mmap-read.o -o $(BIN)5/5.6/mmap-read

$(OBJ)5/5.6/mmap-read.o: $(SRC)5/5.6/mmap-read.c
	gcc -c $(SRC)5/5.6/mmap-read.c -o $(OBJ)5/5.6/mmap-read.o

listing-5.7: $(OBJ)5/5.7/pipe.o
	gcc $(OBJ)5/5.7/pipe.o -o $(BIN)5/5.7/pipe

$(OBJ)5/5.7/pipe.o: $(SRC)5/5.7/pipe.c
	gcc -c $(SRC)5/5.7/pipe.c -o $(OBJ)5/5.7/pipe.o

listing-5.8: $(OBJ)5/5.8/dup2.o
	gcc $(OBJ)5/5.8/dup2.o -o $(BIN)5/5.8/dup2

$(OBJ)5/5.8/dup2.o: $(SRC)5/5.8/dup2.c
	gcc -c $(SRC)5/5.8/dup2.c -o $(OBJ)5/5.8/dup2.o

listing-5.9: $(OBJ)5/5.9/popen.o
	gcc $(OBJ)5/5.9/popen.o -o $(BIN)5/5.9/popen

$(OBJ)5/5.9/popen.o: $(SRC)5/5.9/popen.c
	gcc -c $(SRC)5/5.9/popen.c -o $(OBJ)5/5.9/popen.o

listing-5.10: $(OBJ)5/5.10/socket-server.o
	gcc $(OBJ)5/5.10/socket-server.o -o $(BIN)5/5.10/socket-server

$(OBJ)5/5.10/socket-server.o: $(SRC)5/5.10/socket-server.c
	gcc -c $(SRC)5/5.10/socket-server.c -o $(OBJ)5/5.10/socket-server.o

listing-5.11: $(OBJ)5/5.11/socket-client.o
	gcc $(OBJ)5/5.11/socket-client.o -o $(BIN)5/5.11/socket-client

$(OBJ)5/5.11/socket-client.o: $(SRC)5/5.11/socket-client.c
	gcc -c $(SRC)5/5.11/socket-client.c -o $(OBJ)5/5.11/socket-client.o

listing-5.12: $(OBJ)5/5.12/socket-inet.o
	gcc $(OBJ)5/5.12/socket-inet.o -o $(BIN)5/5.12/socket-inet

$(OBJ)5/5.12/socket-inet.o: $(SRC)5/5.12/socket-inet.c
	gcc -c $(SRC)5/5.12/socket-inet.c -o $(OBJ)5/5.12/socket-inet.o

clean_c5: 
	rm obj/capitulo_5/5.*/*.o
	rm bin/capitulo_5/5.*/*

