#include <pthread.h>
#include <stdio.h>
#include <string.h>
/* An array of balances in accounts, indexed by account number.
*/
float* account_balances;
/* Transfer DOLLARS from account FROM_ACCT to account TO_ACCT. Return
0 if the transaction succeeded, or 1 if the balance FROM_ACCT is
too small. */
int process_transaction (int from_acct, int to_acct, float dollars);
int main(){



float saldos[5];
saldos[0]=6000;
saldos[1]=5000;
saldos[2]=5000;
saldos[3]=300;
saldos[4]=4000;
account_balances=&saldos;
int resultado=process_transaction (0, 3,300.2);
if (resultado==1){

printf("la transaccion no pudo realizarse\n");

}else{

printf("la transaccion se realizo con exito\n");

}





return 0;

}
int process_transaction (int from_acct, int to_acct, float dollars)
{
	int old_cancel_state;
	/* Check the balance in FROM_ACCT. */
	if (account_balances[from_acct] < dollars)
	return 1;
	/* Begin critical section. */
	pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, &old_cancel_state);
	/* Move the money. */
	account_balances[to_acct] += dollars;
	account_balances[from_acct] -= dollars;
	/* End critical section. */
	pthread_setcancelstate (old_cancel_state, NULL);
	return 0;
}
