#include <malloc.h>
#include <pthread.h>
#include <stdio.h>

struct job{
    struct job* next;

    /*otros elementos que caracterizan un trabajo*/
    char trabajo[20];
};

void process_job (struct job*);

/* una lista enlazada de trabajos*/
struct job* job_queue;

//array de hilos
pthread_t hilos[4];

pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

void* thread_function(void* arg){
    while(1){
        struct job* next_job;

        // bloquear mutex
        pthread_mutex_lock(&job_queue_mutex);
        //verificar que la pila este vacia
        if(job_queue == NULL)
            next_job = NULL;
        else{
            //obtener el siguiente trabajo
            next_job = job_queue;
            //remover el trabajo de la cola
            job_queue = job_queue -> next;
        }
        //desbloquear mutex
        pthread_mutex_unlock(&job_queue_mutex);

        //si es que la cola estaba vacia

        if(next_job == NULL)
            break;
        // si es que habia trabajo
        process_job(next_job);

        //limpiar memoria
       // free(next_job);
        return NULL;
    }
}

void process_job (struct job* next_job){
    printf("%s",next_job->trabajo);
}

int main(){
    struct job job1 = {NULL, "limpiando!\n"};
    struct job job2 = {&job1, "cocinando!\n"};
    struct job job3 = {&job2, "arreglando!\n"};
    struct job job4 = {&job3, "ordenando!\n"};
    job_queue = &job4;
    int i;

    printf("creando hilos!\n");
    for(i=0;i<4;i++){
        pthread_create(&hilos[i],NULL,&thread_function,NULL);
    }
   // printf("Esperando hilos");    
    for(i=0;i<4;i++){
        pthread_join(hilos[i],NULL);
    }
}

