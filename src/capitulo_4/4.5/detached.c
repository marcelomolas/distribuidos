#include <pthread.h>
#include <stdio.h>
void* thread_function (void* thread_arg)
{
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
fputc ('x', stderr);
return 0;
}
int main ()
{
pthread_attr_t attr;
pthread_t thread;
pthread_attr_init (&attr);
pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
pthread_create (&thread, &attr, &thread_function, NULL);

pthread_attr_destroy (&attr);
/* Do work here...
*/

pthread_join(thread, NULL);
printf("comprobando la asignacion del atributo PTHREAD_CREATE_DETACHED, lo cual provoca que el hilo no sea esperable \n");
/* No need to join the second thread.*/
return 0;

}
