#include <pthread.h>
#include <thread>

bool should_exit_thread_immediately(int);

class ThreadExitException {
    private: void* thread_return_value_; /*el valor de retorno que se va a usar en la excepcion*/

    public: /*Crea una excepcion que manda una se;al para cerrar un hilo con un valor de retorno */
        ThreadExitException (void* return_value) : thread_return_value_ (return_value) {printf("Thread Exit Exception was called!\n");}
    
    /* salir de la funcion con valor de retorno*/
    void * DoThreadExit (){
        pthread_exit (thread_return_value_);
    }
};

void do_some_work () {
    int sum=0;
    while (1) {
/* Do some useful things here...
*/
    sum++;
    printf("%d\n",sum);
    if (should_exit_thread_immediately (sum))
        throw ThreadExitException (/* thread’s return value = */ NULL);
    }
}

bool should_exit_thread_immediately (int sum){
    if( sum == 1000000)
        return true;
    return false;
}

void* thread_function (void*) {
    try {
        do_some_work ();
    }
    catch (ThreadExitException ex) {
/* Some function indicated that we should exit the thread.*/
        ex.DoThreadExit ();
    }
    return NULL;
}

int main(){
    pthread_t hilo;

    printf("Creando hilo");
    pthread_create(&hilo,NULL,&thread_function,NULL);
    printf("esperando hilo...");
    pthread_join(hilo,NULL);
    return 0;
}