#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;
pthread_t hilos[2];

void do_work();

void initialize_flag(){

    pthread_mutex_init(&thread_flag_mutex,NULL);
    pthread_cond_init(&thread_flag_cv,NULL);

    thread_flag = 0;
}

void* thread_function (void* thread_arg)
{
    while (1) {
        /* Protect the flag with a mutex lock. */
        //printf("hola!");
        pthread_mutex_lock (&thread_flag_mutex);
        while(!thread_flag)
            pthread_cond_wait(&thread_flag_cv,&thread_flag_mutex);
        pthread_mutex_unlock (&thread_flag_mutex);
        
        do_work ();
        
        /* Else don’t do anything.
        Just loop again.
        */
    }
    return NULL;
}
    /* Sets the value of the thread flag to FLAG_VALUE.*/
void set_thread_flag (int flag_value) {
    /* Protect the flag with a mutex lock. */
    pthread_mutex_lock (&thread_flag_mutex);
    thread_flag = flag_value;
    pthread_cond_signal(&thread_flag_cv);
    pthread_mutex_unlock (&thread_flag_mutex);
}

void do_work(){
    printf("haciendo algun trabajo!\n");
//    set_thread_flag(0);
}

int main(){
    int i;
    initialize_flag();
    printf("creando hilo!\n");
    

    pthread_create(&hilos[0],NULL,&thread_function,NULL);
    pthread_create(&hilos[1],NULL,&thread_function,NULL);
    
    set_thread_flag(1);
    sleep(5);
    set_thread_flag(0);
    printf("opa\n");
    exit(0);
    pthread_join(hilos[0],NULL);
    pthread_join(hilos[1],NULL);

    return 0;
}
