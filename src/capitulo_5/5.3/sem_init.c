#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/sem.h>
#include <stdlib.h>
#include <stdio.h> 

/* We must define union semun ourselves. */
union semun {
int val;
struct semid_ds *buf;
unsigned short int *array;
struct seminfo *__buf;
};
/* Initialize a binary semaphore with a value of 1. */
int binary_semaphore_initialize (int semid)
{
union semun argument;
unsigned short values[1];
values[0] = 1;
argument.array = values;
return semctl (semid, 0, SETALL, argument);
}

int main(){
//primero hay que crear un semaforo
key_t llave=IPC_PRIVATE;
int semflg = IPC_CREAT | 0666;
int id = semget (llave, 1, semflg);
int iniBynarySem = binary_semaphore_initialize (id);
printf("Semaforo Inicializado");
return 0;
}

